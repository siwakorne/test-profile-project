import * as admin from 'firebase-admin';

const db = admin.firestore();
const collection = db.collection('profile');

export const authenUser = (username:string, password:string) => new Promise((resolve) => {
  const document = collection.where("username", "==", username).where("password", "==", password).get();
  document.then(snapShot => {
    let returnMessage = 0;
    if(snapShot.empty) {
      returnMessage = -1;
    } else {
      returnMessage = 1;
    }
   resolve(returnMessage);
  }).catch(err => {
    resolve(err);
  })
})

export const loadUser = (username:string) => new Promise((resolve) => {
  const document = collection.where("username", "==", username).get();
  document.then(snapShot => {
    snapShot.forEach(data => {
      data.ref.collection("information").doc("detail").get().then(resp => {
        resolve(resp.data());
      }).catch(err => {
        resolve(err);
      });
    })
  }).catch(err => {
    resolve(err);
  })
})