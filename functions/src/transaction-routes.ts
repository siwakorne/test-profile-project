import { Router } from 'express';
import * as admin from 'firebase-admin';

const db = admin.firestore();

const trans = Router();

trans.route('/success').get((req, res) => {
  const profile = db.collection('profile').doc('user3');
  // let transaction = 
  db.runTransaction(async t => {
    const doc = await t.get(profile);
    if (doc.data() === undefined) {
      return Promise.reject('Fail update');
    }
    const newUsername = doc.data()!.username + '_updated';
    t.update(profile, { username: newUsername });
    console.log('After update username');
    return Promise.resolve('Update username to ' + newUsername);
  }).then(result => {
    const msg = 'Transaction success:' + result;
    console.log(msg);
    res.status(200).send(msg);
  }).catch(err => {
    const msg = 'Transaction failure, ' + err;
    console.log(msg);
    res.status(500).send(msg);
  });
});

trans.route('/fail').get((req, res) => {
  const profile = db.collection('profile').doc('user3');
  // let transaction = 
  db.runTransaction(async t => {
    const doc = await t.get(profile);
    if (doc.data() === undefined) {
      return Promise.reject('Fail update');
    }
    const newUsername = doc.data()!.username + '_updated';
    t.update(profile, { username: newUsername });
    console.log('After update username');
    if (1==1) return Promise.reject('Stub fail update username'); 
    return Promise.resolve('Update username to ' + newUsername);
  }).then(result => {
    const msg = 'Transaction success:' + result;
    console.log(msg);
    res.status(200).send(msg);
  }).catch(err => {
    const msg = 'Transaction failure, ' + err;
    console.log(msg);
    res.status(500).send(msg);
  });
});

trans.route('/fail-throw-error').get((req, res) => {
  const profile = db.collection('profile').doc('user3');
  // let transaction = 
  db.runTransaction(async t => {
    const doc = await t.get(profile);
    if (doc.data() === undefined) {
      return Promise.reject('Fail update');
    }
    const newUsername = doc.data()!.username + '_updated';
    t.update(profile, { username: newUsername });
    console.log('After update username');
    if (1==1) throw new Error('Throw Error');
    return Promise.resolve('Update username to ' + newUsername);
  }).then(result => {
    const msg = 'Transaction success:' + result;
    console.log(msg);
    res.status(200).send(msg);
  }).catch(err => {
    const msg = 'Transaction failure, ' + err;
    console.log(msg);
    res.status(500).send(msg);
  });
});

export default trans;