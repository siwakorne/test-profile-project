import { Router } from 'express';
import * as utils from './utils';

const router = Router();

router.route("/authen").get((req, res) => {
  const username = req.query.username;
  const password = req.query.password;
  utils.authenUser(username, password).then(data => {
    res.status(200).send({status: data});
  }).catch(err => {
    res.status(500).send(err);
  })
})

router.route("/health").get((req, res) => {
  res.status(200).send("The system is working!");
})

router.route("/load-profile").get((req, res) => {
  const username = req.query.username;
  utils.loadUser(username).then(data => {
    res.status(200).send({data})
  }).catch(err => {
    res.status(500).send(err);
  })
})

export default router;