import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp(functions.config().firebase);

import routes from './router';
import * as express from 'express';
const cors = require("cors");
const app = express();

app.use(cors());
app.use(routes);

export const api = functions.https.onRequest(app);
